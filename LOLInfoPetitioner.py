import requests, json

# Ubicar el token en otro lado
RIOT_API_KEY = 'RGAPI-00de7c5e-a397-45ba-afcd-860742602be1'

REGIONS = {
    'br'    :   'br1',
    'eune'  :   'eun1',
    'euw'   :   'euw1',
    'jp'    :   'jp1',
    'kr'    :   'kr',
    'lan'   :   'la1',
    'las'   :   'la2',
    'na'    :   'na1',
    'oce'   :   'oc1',
    'tr'    :   'tr1',
    'ru'    :   'ru',
    'pbe'   :   'pbe1'
}

def requestsSummonerData(region, summonerName):
    URL = "https://" + region + ".api.riotgames.com/lol/summoner/v3/summoners/by-name/" + summonerName + "?api_key=" + RIOT_API_KEY
    response = requests.get(URL)
    return response.json()

def requestRankedData(region, summonerId):
    URL = "https://" + region + ".api.riotgames.com/lol/league/v3/positions/by-summoner/" + summonerId + "?api_key=" + RIOT_API_KEY
    print URL
    response = requests.get(URL)
    return response.json()

def getServicePlatformByServiceRegion(region):
    return REGIONS[region]
    

def requestInfo(region, summonerName):
    region = getServicePlatformByServiceRegion(region)

    summonerData = requestsSummonerData(region, summonerName)
    
    summonerId = summonerData['id']
    summonerId = str(summonerId)

    rankedData = requestRankedData(region, summonerId)

    info =  (
        'Nombre de Invocador: ' + summonerData['name'] + '\n' +
        'Nivel de Invocador: ' + str(summonerData['summonerLevel'])
    )

    if len(rankedData) == 0:
        info +=  (
            '\n\n' + 
            'No hay informacion sobre las colas de clasificatoria'
        )

    if len(rankedData) > 0:
        info +=  (
            '\n\n' + 
            'Cola: ' + rankedData[0]['queueType'] + '\n' +
            'Nombre de Liga: ' + rankedData[0]['leagueName'] + '\n' +
            'Liga: ' + rankedData[0]['tier'] + '\n' +
            'Division: ' + rankedData[0]['rank'] + '\n' +
            'Partidas Ganadas: ' + str(rankedData[0]['wins']) + '\n' +
            'Partidas Perdidas: ' + str(rankedData[0]['losses']) + '\n' +
            'Puntos de Liga: ' + str(rankedData[0]['leaguePoints'])
        )

    if len(rankedData) > 1:
        info +=  (
            '\n\n' +
            'Cola: ' + rankedData[1]['queueType'] + '\n' +
            'Nombre de Liga: ' + rankedData[1]['leagueName'] + '\n' +
            'Liga: ' + rankedData[1]['tier'] + '\n' +
            'Division: ' + rankedData[1]['rank'] + '\n' +
            'Partidas Ganadas: ' + str(rankedData[1]['wins']) + '\n' +
            'Partidas Perdidas: ' + str(rankedData[1]['losses']) + '\n' +
            'Puntos de Liga: ' + str(rankedData[1]['leaguePoints'])
        )

    return str(info)